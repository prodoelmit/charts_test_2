#include "yseriessettingswidget.h"
#include "chartapp.h"

YSeriesSettingsWidget::YSeriesSettingsWidget()
    : QWidget()
{

    m_layout = new QGridLayout();
    setLayout(m_layout);


    m_columnLabel = new QLabel("columnId");
    m_columnIdEdit = new QSpinBox();
    m_columnIdEdit->setRange(0, 1000);
    m_columnIdEdit->setSingleStep(1);

    m_columnCB = new QComboBox;
    refillColumnCombobox();

    connect(m_columnCB, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int i) {
        m_columnIdEdit->setValue(i);
        app->refreshSettings(true);
        emit columnChanged(i);
    });


    connect(m_columnIdEdit, &QSpinBox::editingFinished, [this]() {
        int i = m_columnIdEdit->value();
        m_columnCB->setCurrentIndex(i);
        emit columnChanged(i);
//		app->refreshSettings();
    });

    m_removeSeriesButton = new QPushButton("Remove this series");

    m_layout->addWidget(m_columnLabel, 0, 0);
    m_layout->addWidget(m_columnIdEdit, 0, 1);
    m_layout->addWidget(m_columnCB, 0, 2);
    m_layout->addWidget(m_removeSeriesButton, 1, 0, 1, 3);
    connect(m_removeSeriesButton, &QPushButton::clicked,
            [this]() {
        this->deleteLater();
    });


}

YSeriesSettingsWidget::YSeriesSettingsWidget(int i)
    : YSeriesSettingsWidget()
{

    m_columnIdEdit->setValue(i);

}

int YSeriesSettingsWidget::colId()
{
    return m_columnIdEdit->value();

}

void YSeriesSettingsWidget::refillColumnCombobox()
{
    int curIndex = m_columnCB->currentIndex();
    m_columnCB->clear();
    if (chrt->hasHeaders()) {
        m_columnCB->addItems(chrt->headers());
    }

    if (m_columnCB->count() && curIndex < 0) {
        curIndex = m_columnIdEdit->value();
    }
    m_columnCB->setCurrentIndex(curIndex);



}
