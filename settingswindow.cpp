#include "settingswindow.h"
#include "chartapp.h"
#include "qdebug.h"
#include "chart.h"
#include <QFile>
#include <QComboBox>

SettingsWindow::SettingsWindow(QString settingsPath)
	: QWidget()
	, m_canvasColorDialog(0)
{
	m_cLayout = new QGridLayout; setLayout(m_cLayout);

	// <canvas settings group>
	m_canvasGroup = new QGroupBox("Canvas");
	m_canvasGroupLayout = new QGridLayout();
	m_canvasGroup->setLayout(m_canvasGroupLayout);
	m_cLayout->addWidget(m_canvasGroup, 0, 0);

	m_sizeLabel = new QLabel("Size");
	m_sizeXEdit = new QSpinBox();
	m_sizeXEdit->setRange(100, 3000);
	m_sizeXEdit->setValue(800);
	m_sizeYEdit = new QSpinBox(); m_sizeYEdit->setRange(100, 3000);
	m_sizeYEdit->setValue(600);

	m_canvasGroupLayout->addWidget(m_sizeLabel, 0, 0);
	m_canvasGroupLayout->addWidget(m_sizeXEdit, 0, 1);
	m_canvasGroupLayout->addWidget(m_sizeYEdit, 0, 2);

	m_canvasColorLabel = new QLabel("Color");
	m_canvasColorButton = new QPushButton;
	m_canvasColorButton->setStyleSheet(QString("background-color: ") + chrt->backgroundBrush().color().name() + ";");
	connect(m_canvasColorButton, &QPushButton::clicked, [&](){
		if (m_canvasColorDialog) {m_canvasColorDialog->setFocus();return;}
		auto prevBrush = chrt->backgroundBrush();
		m_canvasColorDialog = new QColorDialog(chrt->backgroundBrush().color());
		auto d = m_canvasColorDialog;
		connect(d, &QColorDialog::currentColorChanged, [d, this](){
			auto c = d->currentColor();
			QBrush b(chrt->backgroundBrush());
			b.setColor(c);
			chrt->setBackgroundBrush(b);
			m_canvasColorButton->setStyleSheet(QString("background-color: ")  + c.name()  +  ";");
			app->refresh();
		});

		d->show();

		connect(d, &QColorDialog::finished, [this, d, prevBrush]() {
			if (!d->result()) {
				auto b = prevBrush;
				chrt->setBackgroundBrush(b);
				QColor c = b.color();
				m_canvasColorButton->setStyleSheet(QString("background-color: ")  + c.name()  +  ";");
			}
//				delete m_canvasColorDialog;

			m_canvasColorDialog->deleteLater();
				m_canvasColorDialog = 0;

		});

	});

	m_canvasGroupLayout->addWidget(m_canvasColorLabel, 1, 0);
	m_canvasGroupLayout->addWidget(m_canvasColorButton, 1, 1, 1, 2);



	connect(m_sizeXEdit, SIGNAL(editingFinished()), app, SLOT(refreshSettings()));
	connect(m_sizeYEdit, SIGNAL(editingFinished()), app, SLOT(refreshSettings()));



	m_axesFontSizeLabel = new QLabel("AxesFontSize");
	m_canvasGroupLayout->addWidget(m_axesFontSizeLabel, 2, 0);

	m_axesFontSizeEdit = new QDoubleSpinBox();
	m_axesFontSizeEdit->setRange(0.0001, 1000);
	m_axesFontSizeEdit->setValue(10);
	m_axesFontSizeEdit->setSingleStep(0.05);

	m_canvasGroupLayout->addWidget(m_axesFontSizeEdit, 2, 1);

	connect(m_axesFontSizeEdit, SIGNAL(valueChanged(double)), app, SLOT(refreshSettings()));

	m_legendFontSizeLabel = new QLabel("legendFontSize");
	m_canvasGroupLayout->addWidget(m_legendFontSizeLabel, 3, 0);

	m_legendFontSizeEdit = new QDoubleSpinBox();
	m_legendFontSizeEdit->setRange(0.0001, 1000);
	m_legendFontSizeEdit->setValue(10);
	m_legendFontSizeEdit->setSingleStep(0.05);

	m_canvasGroupLayout->addWidget(m_legendFontSizeEdit, 3, 1);

	connect(m_legendFontSizeEdit, SIGNAL(valueChanged(double)), app, SLOT(refreshSettings()));


	m_legendAlignmentLabel = new QLabel("LegendAlignment");
	m_canvasGroupLayout->addWidget(m_legendAlignmentLabel, 4, 0);

	m_legendAlignmentEdit = new QComboBox();
	m_legendAlignmentEdit->addItem("Left", Qt::AlignLeft);
	m_legendAlignmentEdit->addItem("Top", Qt::AlignTop);
	m_legendAlignmentEdit->addItem("Bottom", Qt::AlignBottom);
	m_legendAlignmentEdit->addItem("Right", Qt::AlignRight);

	m_canvasGroupLayout->addWidget(m_legendAlignmentEdit, 4, 1);

	connect(m_legendAlignmentEdit, SIGNAL(currentIndexChanged(int)), app, SLOT(refreshSettings()));





	// </canvas settings group>


	// <xAxis settings group>
	m_xAxisGroup = new QGroupBox("xAxis");
	m_xAxisGroupLayout = new QGridLayout(m_xAxisGroup);

	m_cLayout->addWidget(m_xAxisGroup, 1, 0);



	m_xColumnIdLabel = new QLabel("columnId");

	m_xColumnIdEdit = new QSpinBox();
	m_xColumnIdEdit->setRange(0, 1000);
	m_xColumnIdEdit->setSingleStep(1);

	m_xColumnIdCB = new QComboBox;
	refillXColumnCombobox();


	connect(m_xColumnIdCB, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int i) {
		m_xColumnIdEdit->setValue(i);
		app->refreshSettings(true);
	});

	connect(m_xColumnIdEdit, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](int i) {
		m_xColumnIdCB->setCurrentIndex(i);
//		app->refreshSettings();
	});


	m_xAxisGroupLayout->addWidget(m_xColumnIdLabel, 0, 0);
	m_xAxisGroupLayout->addWidget(m_xColumnIdEdit, 0, 1);
	m_xAxisGroupLayout->addWidget(m_xColumnIdCB, 0, 2);

	m_xMajorTicksCountLabel = new QLabel("MajorTicksCount");
	m_xMinorTicksCountLabel = new QLabel("MinorTicksCount");

	m_xMajorTicksCountEdit = new QSpinBox();
	m_xMajorTicksCountEdit->setRange(0, 20);
	m_xMajorTicksCountEdit->setValue(dynamic_cast<QValueAxis*>(chrt->axisX())->tickCount());
	m_xMajorTicksVisibleEdit = new QCheckBox("Visible");
	m_xMajorTicksVisibleEdit->setChecked(
	dynamic_cast<QValueAxis*>(chrt->axisX())->isGridLineVisible()
				);

	m_xMinorTicksCountEdit = new QSpinBox();
	m_xMinorTicksCountEdit->setRange(0, 20);
	m_xMinorTicksCountEdit->setValue(dynamic_cast<QValueAxis*>(chrt->axisX())->minorTickCount());
	m_xMinorTicksVisibleEdit = new QCheckBox("Visible");
	m_xMinorTicksVisibleEdit->setChecked(
	dynamic_cast<QValueAxis*>(chrt->axisX())->isMinorGridLineVisible()
				);

	m_xAxisGroupLayout->addWidget(m_xMajorTicksCountLabel, 1, 0);
	m_xAxisGroupLayout->addWidget(m_xMajorTicksCountEdit, 1, 1);
	m_xAxisGroupLayout->addWidget(m_xMajorTicksVisibleEdit, 1, 2);

	m_xAxisGroupLayout->addWidget(m_xMinorTicksCountLabel, 2, 0);
	m_xAxisGroupLayout->addWidget(m_xMinorTicksCountEdit, 2, 1);
	m_xAxisGroupLayout->addWidget(m_xMinorTicksVisibleEdit, 2, 2);


	connect(m_xMajorTicksCountEdit, SIGNAL(valueChanged(int)), app, SLOT(refreshSettings()));
	connect(m_xMinorTicksCountEdit, SIGNAL(valueChanged(int)), app, SLOT(refreshSettings()));
	connect(m_xMajorTicksVisibleEdit, SIGNAL(stateChanged(int)), app, SLOT(refreshSettings()));
	connect(m_xMinorTicksVisibleEdit, SIGNAL(stateChanged(int)), app, SLOT(refreshSettings()));


	m_xLabelLabel = new QLabel("Label");
	m_xLabelEdit = new QLineEdit();
	m_xLabelEdit->setText(chrt->axisX()->titleText());

	m_xAxisGroupLayout->addWidget(m_xLabelLabel, 3, 0);
	m_xAxisGroupLayout->addWidget(m_xLabelEdit, 3, 1);

	connect(m_xLabelEdit, SIGNAL(editingFinished()), app, SLOT(refreshSettings()));
	// </xAxis settings group>




	// <shortcuts>

	QShortcut* ctrls = new QShortcut(QKeySequence("Ctrl+S"), this);
	connect(ctrls, SIGNAL(activated()), this, SLOT(exportFile()));

	QShortcut* ctrlo = new QShortcut(QKeySequence("Ctrl+O"), this);
	connect(ctrlo, SIGNAL(activated()), this, SLOT(importFile()));
	// </shortcuts>

    if (settingsPath.length()) {
        chrt->loadSettings(settingsPath);
        updateSettings();
//        app->refresh();
    }

    // <ySeries>

    m_ySeriesGroupBox = new QGroupBox("ySeries");
    m_ySeriesGroupBoxLayout = new QGridLayout;
    m_ySeriesGroupBox->setLayout(m_ySeriesGroupBoxLayout);


    m_ySeriesWidgetsCB = new QComboBox();
    m_ySeriesWidgets = new QStackedWidget();

    int counter = 0;
    foreach (auto i, chrt->yCols()) {
        auto sw = new YSeriesSettingsWidget(i);
        m_ySeriesWidgetsCB->addItem(chrt->headers().at(i), QVariant::fromValue(sw));
        m_ySeriesWidgets->addWidget(sw);
        connect(sw, &YSeriesSettingsWidget::destroyed,
                [counter,this](){
                    m_ySeriesWidgetsCB->removeItem(counter);
                    m_ySeriesWidgets->removeWidget(m_ySeriesWidgets->widget(counter));
                    app->refreshSettings(true);
                });
        connect(sw, &YSeriesSettingsWidget::columnChanged,
                [this,sw](int colId) {
            int index = m_ySeriesWidgetsCB->findData(QVariant::fromValue(sw));
            m_ySeriesWidgetsCB->setItemText(index, chrt->headers().at(colId));
        });
        counter++;
    }

    m_addYSeriesWidgetButton = new QPushButton("Add series");

    connect(m_addYSeriesWidgetButton, &QPushButton::clicked,
            [this]() {

        auto sw = new YSeriesSettingsWidget(0);
        m_ySeriesWidgetsCB->addItem(chrt->headers().at(0), QVariant::fromValue(sw));
        m_ySeriesWidgets->addWidget(sw);
        int counter = m_ySeriesWidgetsCB->count() - 1;
        connect(sw, &YSeriesSettingsWidget::destroyed,
                [counter,this](){
                    m_ySeriesWidgetsCB->removeItem(counter);
                    m_ySeriesWidgets->removeWidget(m_ySeriesWidgets->widget(counter));
                    app->refreshSettings(true);
                });
        connect(sw, &YSeriesSettingsWidget::columnChanged,
                [this,sw](int colId) {
            int index = m_ySeriesWidgetsCB->findData(QVariant::fromValue(sw));
            m_ySeriesWidgetsCB->setItemText(index, chrt->headers().at(colId));
        });
    });

    connect(m_ySeriesWidgetsCB, (static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged)),
            [this](int index){
        m_ySeriesWidgets->setCurrentIndex(index);

    });


    m_cLayout->addWidget(m_ySeriesGroupBox, 2, 0);


    m_ySeriesGroupBoxLayout->addWidget(m_ySeriesWidgetsCB, 0, 0);
    m_ySeriesGroupBoxLayout->addWidget(m_addYSeriesWidgetButton, 0, 1);
    m_ySeriesGroupBoxLayout->addWidget(m_ySeriesWidgets, 1, 0, 1, 2);


    // </ySeries>

}

QSize SettingsWindow::canvasSize() const
{
	return QSize( m_sizeXEdit->value()
				, m_sizeYEdit->value()
				  );
}

qreal SettingsWindow::axesFontPointSize() const
{
	return m_axesFontSizeEdit->value();
}

qreal SettingsWindow::legendFontPointSize() const
{
	return m_legendFontSizeEdit->value();
}

Qt::Alignment SettingsWindow::legendAlignment() const
{
	return (Qt::Alignment)(m_legendAlignmentEdit->currentData().toInt());
}

void SettingsWindow::importFile()
{

	QString filename = QFileDialog::getOpenFileName();
	if (!filename.length()) {
		return;
	}

	chrt->loadSettings(filename);
	updateSettings();
	app->refresh();



}

QStackedWidget *SettingsWindow::ySeriesWidgets() const
{
    return m_ySeriesWidgets;
}


void SettingsWindow::importCSV()
{
    auto filename = QFileDialog::getOpenFileName();
	QFile file(filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) {
		qDebug() << "error opening file " << filename << " \n Reason: " << file.errorString();
	}
}

void SettingsWindow::refillXColumnCombobox()
{
	int curIndex = m_xColumnIdCB->currentIndex();
	m_xColumnIdCB->clear();
	if (chrt->hasHeaders()) {
		m_xColumnIdCB->addItems(chrt->headers());
	}

	if (m_xColumnIdCB->count() && curIndex < 0) {
		curIndex = m_xColumnIdEdit->value();
	}
	m_xColumnIdCB->setCurrentIndex(curIndex);

}

void SettingsWindow::updateSettings()
{
    qDebug() << "happened with me";
	app->blockRefreshing(true);
	QValueAxis* xAxis = dynamic_cast<QValueAxis*>(chrt->axisX());
	QValueAxis* yAxis = dynamic_cast<QValueAxis*>(chrt->axisY());

	// <canvas>
	QSize size = chrt->size().toSize();
	m_sizeXEdit->setValue(size.width());
	m_sizeYEdit->setValue(size.height());

	m_canvasColorButton->setStyleSheet(QString("background-color: ") + chrt->backgroundBrush().color().name() + ";");

	m_axesFontSizeEdit->setValue(xAxis->labelsFont().pointSizeF());

	m_legendFontSizeEdit->setValue(chrt->legend()->font().pointSizeF());

	m_legendAlignmentEdit->setCurrentIndex(m_legendAlignmentEdit->findData((int)chrt->legend()->alignment()));
	// </canvas>

	// <xAxis>
	m_xColumnIdEdit->setValue(chrt->xCol());

	m_xMajorTicksCountEdit->setValue(xAxis->tickCount());
	m_xMinorTicksCountEdit->setValue(xAxis->minorTickCount());
	m_xMajorTicksVisibleEdit->setChecked(xAxis->isGridLineVisible());
	m_xMinorTicksVisibleEdit->setChecked(xAxis->isMinorGridLineVisible());

	m_xLabelEdit->setText(xAxis->titleText());

	app->blockRefreshing(false);
	// </xAxis>


}

int SettingsWindow::xCol() const
{
	return m_xColumnIdEdit->value();
}

int SettingsWindow::xMajorTicksCount() const
{
	return m_xMajorTicksCountEdit->value();
}

int SettingsWindow::xMinorTicksCount() const
{
	return m_xMinorTicksCountEdit->value();
}

bool SettingsWindow::xMajorTicksVisible() const
{
	return m_xMajorTicksVisibleEdit->checkState() == Qt::Checked;
}

bool SettingsWindow::xMinorTicksVisible() const
{
	return m_xMinorTicksVisibleEdit->checkState() == Qt::Checked;
}

QString SettingsWindow::xLabel() const
{
	return m_xLabelEdit->text();
}

void SettingsWindow::exportFile()
{
	auto filename = QFileDialog::getSaveFileName();
	chrt->saveSettings(filename);
}
