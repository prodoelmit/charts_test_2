#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H
#include <QStackedWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QSpinBox>
#include <QLabel>
#include <QColorDialog>
#include <QComboBox>
#include <QCheckBox>
#include "yseriessettingswidget.h"


class SettingsWindow: public QWidget
{ Q_OBJECT
public:
    SettingsWindow(QString settingsPath = QString());

	QSize canvasSize() const;
	qreal axesFontPointSize() const;
	qreal legendFontPointSize() const;
	Qt::Alignment legendAlignment() const;
	void importCSV();
	void refillXColumnCombobox();
	void updateSettings();

	int xCol() const;
	int xMajorTicksCount() const;
	int xMinorTicksCount() const;
	bool xMajorTicksVisible() const;
	bool xMinorTicksVisible() const;
	QString xLabel() const;

    QStackedWidget *ySeriesWidgets() const;

public slots: void exportFile();
    void importFile();

private:
	QGridLayout* m_cLayout;

	//<canvas>
	QGroupBox* m_canvasGroup;
	QGridLayout* m_canvasGroupLayout;

	QLabel* m_sizeLabel;
	QSpinBox* m_sizeXEdit;
	QSpinBox* m_sizeYEdit;

	QColorDialog* m_canvasColorDialog;
	QLabel* m_canvasColorLabel;
	QPushButton* m_canvasColorButton;

	QLabel* m_axesFontSizeLabel;
	QDoubleSpinBox* m_axesFontSizeEdit;

	QLabel* m_legendFontSizeLabel;
	QDoubleSpinBox* m_legendFontSizeEdit;

	QLabel* m_legendAlignmentLabel;
	QComboBox* m_legendAlignmentEdit;

	//</canvas>

	//<xAxis>

	QGroupBox* m_xAxisGroup;
	QGridLayout* m_xAxisGroupLayout;

	QLabel* m_xColumnIdLabel;
	QSpinBox* m_xColumnIdEdit;
	QComboBox* m_xColumnIdCB;


	QLabel* m_xMajorTicksCountLabel;
	QSpinBox* m_xMajorTicksCountEdit;
	QCheckBox* m_xMajorTicksVisibleEdit;

	QLabel* m_xMinorTicksCountLabel;
	QSpinBox* m_xMinorTicksCountEdit;
	QCheckBox* m_xMinorTicksVisibleEdit;

	QLabel* m_xLabelLabel;
	QLineEdit* m_xLabelEdit;

    QGroupBox* m_ySeriesGroupBox;
    QGridLayout* m_ySeriesGroupBoxLayout;
    QComboBox* m_ySeriesWidgetsCB;
    QPushButton* m_addYSeriesWidgetButton;
//    QVector<YSeriesSettingsWidget*> m_ySeriesWidgets;
    QStackedWidget* m_ySeriesWidgets;


	//</xAxis>


};

#endif // SETTINGSWINDOW_H
