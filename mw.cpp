#include "mw.h"
#include "chartapp.h"
#include <QPixmap>
#include <QImage>

#include <QGridLayout>
#include <QSvgGenerator>

MW::MW(QWidget *parent) :
	QMainWindow(parent)
{
	QWidget* cWidget = new QWidget;
	setCentralWidget(cWidget);
	QGridLayout* gridLayout = new QGridLayout;
	cWidget->setLayout(gridLayout);

	m_view = new QChartView();
	gridLayout->addWidget(m_view, 0,0);


	QChart* chart = app->chart();
	m_view->setChart(chart);
	chart->resize(800,600);

	QShortcut* ccs = new QShortcut(QKeySequence("Ctrl+C"), this);
	connect(ccs, SIGNAL(activated()), this, SLOT(copySVGToClipboard()));
}

MW::~MW()
{
}


void MW::copySVGToClipboard()
{
//	qDebug() << "SVG";
//	QSize s = app->chart()->size().toSize();
//	QBuffer b;
////	QSvgGenerator p;
//	QImage p(s, QImage::Format_RGB16);
//	p.fill(Qt::white);
////	p.setOutputDevice(&b);
////	p.setSize(s);
////	p.setViewBox(QRect(0,0,s.width(),s.height()));
//	QPainter painter;
//	painter.begin(&p);
//	painter.setRenderHint(QPainter::Antialiasing);
////	ui->chartWidget->resize(600,320);
////	app->chart()->paint(&painter, 0, 0);
//	app->chart()->scene()->render(&painter);
////	m_view->render(&painter);
//	qDebug() << "Copied";
//	painter.end();
////	QMimeData * d = new QMimeData();
////	d->setData("image/svg+xml",b.buffer());
////	QApplication::clipboard()->setMimeData(d,QClipboard::Clipboard);
//	QApplication::clipboard()->setImage(p, QClipboard::Clipboard);

}
