#include "chartapp.h"
#include "qdebug.h"
#include <QSvgGenerator>


ChartApp::ChartApp(int &argc, char **argv)
	: QApplication(argc, argv)
	, m_chartSize(QSize(800,500))
	, m_chartRect(QRect(0,0,800,600))
	, m_sep(QString(","))
    , m_blockRefreshing(false)
    , m_xCol(0)
    , m_launchGui(false)
{
	bool launchGui = false;

	for (int i = 0; i < argc; i++) {
		if (QString(argv[i]) == "--gui") {
			qDebug() << "gui";
            m_launchGui = true; } else if (QString(argv[i]) == "-i") {

			if (i < argc - 1) {
				m_inPath = QString(argv[i+1]);
			}
		} else if (QString(argv[i]) == "-o") {
			if (i < argc - 1) {
				m_outPath = QString(argv[i+1]);
			}
		} else if (QString(argv[i]) == "-s" || QString(argv[i]) == "--separator") {
			if (i < argc - 1) {
				m_sep = argv[i+1];
			}
		} else if (QString(argv[i]) == "-t" || QString(argv[i]) == "--title") {
			if (i < argc - 1) {
				m_title = argv[i+1];
			}
		} else if (QString(argv[i]) == "-x") {
			if (i < argc - 1) {
				m_xCol = QString(argv[i+1]).toInt();
			}
		} else if (QString(argv[i]) == "-y") {
			if (i < argc - 1) {
				QStringList list = QString(argv[i+1]).split(',');
				foreach (QString s, list) {
					m_yCols << s.toInt();
				}
			}
        } else if (QString(argv[i]) == "--settings") {
            if (i < argc - 1) {
                m_settingsPath = argv[i + 1];
            }
        }
	}



	m_chart = new Chart();
	if (m_inPath.length()) {
		QDir dir = QDir::current();
		QString inPathGlobal = dir.absoluteFilePath(m_inPath);
		m_chart->readCSV(inPathGlobal, true, ";");
		m_chart->setTitle(m_title);
		m_chart->setXColumn(m_xCol);
        if (m_yCols.empty()) {m_yCols << 0;}
		m_chart->setYColumns(m_yCols);
		m_chart->prepareChart();
	} else {
		QLineSeries* lSeries = new QLineSeries();
		lSeries->append(0,0);
		lSeries->append(1,1);
		lSeries->append(2,4);
		lSeries->append(3,9);
		m_chart->addSeries(lSeries);
		m_chart->createDefaultAxes();
		lSeries->attachAxis(m_chart->axisX());
		lSeries->attachAxis(m_chart->axisY());
	}







    if (m_settingsPath.length()) {
        m_chart->loadSettings(m_settingsPath);
    }
    if (m_launchGui) {
        m_chart->resize(m_chartSize);
        m_mw = new SettingsWindow(m_settingsPath);
		m_mw->show();
	} else {
	}


}

ChartApp *ChartApp::instance()
{
	return dynamic_cast<ChartApp*>(qApp->instance());
}

QSize ChartApp::chartSize() const
{
	return m_chartSize;
}

void ChartApp::setChartSize(const QSize &chartSize)
{
	m_chartSize = chartSize;
	chart()->resize(chartSize);
}

QRect ChartApp::chartRect() const
{
	return m_chartRect;
}

void ChartApp::setChartRect(const QRect &chartRect)
{
	m_chartRect = chartRect;
}

Chart *ChartApp::chart() const
{
	return m_chart;
}

void ChartApp::blockRefreshing(bool b)
{
	m_blockRefreshing = b;
}

void ChartApp::run()
{
    if (m_launchGui) {
        m_view = new QGraphicsView();
        QShortcut* ctrlc = new QShortcut(QKeySequence("Ctrl+C"), m_view );
        connect(ctrlc, SIGNAL(activated()), this, SLOT(copy()));
        QShortcut* ctrlr = new QShortcut(QKeySequence("Ctrl+R"), m_view );
        connect(ctrlr, SIGNAL(activated()), this, SLOT(refresh()));
        m_view->show();
        m_view->setGeometry(0,0,500,500);
        m_chart->render(m_view);
        m_view->setRenderHint(QPainter::Antialiasing);
    //		m_chart->svgToClipboard();
        //		qApp->exit();

        if (m_outPath.length()) {
            m_chart->pngToFile(m_outPath);
        }

        m_chart->saveSettings();
    } else if (m_outPath.length()) {
        m_chart->pngToFile(m_outPath);
        quit();
    }
}

void ChartApp::copy()
{
//	m_chart->svgToClipboard();
	m_chart->pngToClipboard();
}

void ChartApp::refresh()
{
	m_chart->render(m_view);

}

void ChartApp::refreshSettings(bool replot)
{
	if (m_blockRefreshing) return;
	// <canvas>
	m_chart->resize(m_mw->canvasSize());

	auto axesFont = m_chart->axisX()->labelsFont();
	axesFont.setPointSizeF(m_mw->axesFontPointSize());
	m_chart->axisX()->setLabelsFont(axesFont);
	m_chart->axisY()->setLabelsFont(axesFont);

	auto alignment = m_mw->legendAlignment();

	m_chart->legend()->setAlignment(alignment);

	auto legendFont = m_chart->legend()->font();
	legendFont.setPointSizeF(m_mw->legendFontPointSize());
	m_chart->legend()->setFont(legendFont);

	// </canvas>

	// <xAxis>

	int xColumnId = m_mw->xCol();
	qDebug() << xColumnId  << m_chart->headers().size() << m_chart->headers();
	if (xColumnId < m_chart->headers().size()) {
		m_chart->setXColumn(xColumnId);
	}

	int xMajorTicksCount = m_mw->xMajorTicksCount();
	int xMinorTicksCount = m_mw->xMinorTicksCount();

	dynamic_cast<QValueAxis*>(m_chart->axisX())->setTickCount(xMajorTicksCount);
	dynamic_cast<QValueAxis*>(m_chart->axisX())->setMinorTickCount(xMinorTicksCount);

	bool xMajorTicksVisible = m_mw->xMajorTicksVisible();
	bool xMinorTicksVisible = m_mw->xMinorTicksVisible();

	dynamic_cast<QValueAxis*>(m_chart->axisX())->setGridLineVisible(xMajorTicksVisible);
	dynamic_cast<QValueAxis*>(m_chart->axisX())->setMinorGridLineVisible(xMinorTicksVisible);


	QString xLabel = m_mw->xLabel();
	dynamic_cast<QValueAxis*>(m_chart->axisX())->setTitleText(xLabel);



	// </xAxis>

    // <ySeries>
    {
        QStackedWidget* sw = m_mw->ySeriesWidgets();
        int count = sw->count();

        QList<int> yCols;

        for (int i = 0; i < count; i++) {
            auto w = (YSeriesSettingsWidget*)sw->widget(i);
            int id = w->colId();
            yCols << id;
        }
        chrt->setYCols(yCols);



    }

    // </ySeries>


	if (replot) m_chart->prepareChart();
	refresh();

}
