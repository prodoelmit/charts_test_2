#ifndef MW_H
#define MW_H
#include <QGraphicsView>
#include <QChartView>

#include <QMainWindow>
using namespace QtCharts;


class MW : public QMainWindow
{
	Q_OBJECT

public:
	explicit MW(QWidget *parent = 0);
	~MW();
public slots:

	void copySVGToClipboard();
private:
	QChartView* m_view;
};

#endif // MW_H
