#-------------------------------------------------
#
# Project created by QtCreator 2016-08-05T15:11:15
#
#-------------------------------------------------

QT       += core gui charts svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = charts_test_2
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp \
    chartapp.cpp \
    settingswindow.cpp \
    yseriessettingswidget.cpp

HEADERS  += mw.h \
    chartapp.h \
    settingswindow.h \
    yseriessettingswidget.h

FORMS    += mw.ui

unix:!macx: LIBS += -L$$OUT_PWD/../QLibChart/ -lQLibChart
win32: LIBS += -L$$OUT_PWD/../QLibChart/ -lQLibChart

INCLUDEPATH += $$PWD/../QLibChart
DEPENDPATH += $$PWD/../QLibChart
