#ifndef YSERIESSETTINGSWIDGET_H
#define YSERIESSETTINGSWIDGET_H
#include <QPushButton>
#include <QWidget>
#include <QGridLayout>
#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QAbstractSeries>
using namespace QtCharts;



class YSeriesSettingsWidget: public QWidget
{
    Q_OBJECT
public:
    YSeriesSettingsWidget();
    YSeriesSettingsWidget(int i);
    int colId();

signals:
    void columnChanged(int i);

private:
    void refillColumnCombobox();
    QGridLayout* m_layout;

    QLabel* m_columnLabel;
    QComboBox* m_columnCB;
    QSpinBox* m_columnIdEdit;

    QAbstractSeries* m_series;

    QPushButton* m_removeSeriesButton;

};

#endif // YSERIESSETTINGSWIDGET_H
