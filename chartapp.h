#ifndef CHARTAPP_H
#define CHARTAPP_H
#include <QApplication>
#include <QMainWindow>
#include "settingswindow.h"
#include <QtCharts>
#include "mw.h"
#include "chart.h"
using namespace QtCharts;



class ChartApp: public QApplication
{
	Q_OBJECT
public:
	ChartApp(int& argc, char** argv);

	ChartApp* instance();

	QSize chartSize() const;
	void setChartSize(const QSize &chartSize);

	QRect chartRect() const;
	void setChartRect(const QRect &chartRect);

	Chart *chart() const;
	void blockRefreshing(bool b);


public slots:
	void run();
	void copy();
	void refresh();
	void refreshSettings(bool replot = false);


private:

	SettingsWindow* m_mw;
	Chart* m_chart;
	QSize m_chartSize;
	QRect m_chartRect;
	QGraphicsScene* m_scene;
	QGraphicsView* m_view;

	bool m_launchGui;
	QString m_inPath;
	QString m_outPath;
	QString m_sep;
	QString m_title;
	int m_xCol;
	QList<int> m_yCols;
	qreal normalAxisLabelsFontSize;

	bool m_blockRefreshing;
    QString m_settingsPath;





};

#define app dynamic_cast<ChartApp*>(qApp->instance())->instance()
#define chrt app->chart()

#endif // CHARTAPP_H
