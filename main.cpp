#include "mw.h"
#include <QApplication>
#include "chartapp.h"
#include <QTimer>

int main(int argc, char *argv[])
{
	ChartApp a(argc, argv);
	QTimer::singleShot(0, &a, SLOT(run()));

	return a.exec();
}
